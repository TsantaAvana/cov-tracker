import React from 'react';
import styled from 'styled-components';
import CardData from './card';
import Chart from './chart';
import CountryPicker from './countryPicker';
import { fetchData } from '../api/index';
import Navs from "./nav";
import {device} from '../style/device';


const StyledContainer = styled.div`

  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
 
`;

const StyledBigContainer = styled.div`
  position: relative;
  overflow: hidden;
`;

const StyledFooter = styled.div`
  position: absoulute;
  bottom:0;
  
`;


const StyledLefTopGradient = styled.div`
  position: absolute;
  top: -35%;
  right: -20%;
  width: 800px;
  height: 800px;
  border-radius: 50%;
  background: linear-gradient(#ee9ca7, #ec4357);
  ${device.bigDesktop`
        position: absolute;
        width: 400px;
        height: 400px;
        top: -10%;
        right: -10%;

    `};
  ${device.tablet`
        position: absolute;
        width: 400px;
        height: 400px;
        top: -5%;
        right: -20%;

    `};
  ${device.phone`
        position: absolute;
        width: 300px;
        height: 300px;
        top: -5%;
        right: -20%;

    `};
  ${device.tiny`
        position: absolute;
        width: 300px;
        height: 300px;
        top: -5%;
        right: -20%;

    `};
  ${device.phablet`
        position: absolute;
        width: 300px;
        height: 300px;
        top: -5%;
        right: -20%;

    `};
`;


class Statistique  extends React.Component{

    state = {
        data:{},
        country: '',
    }

    async componentDidMount(){

        const fetchedData = await fetchData();
        this.setState({ data: fetchedData});
    }

    handleCountryChange = async (country) => {
       
        const fetchedData = await fetchData(country);
        this.setState({ data: fetchedData, country: country });
       
    }

    render() {
        const { data, country } = this.state;
    return (
      <>
        <StyledBigContainer>
          <Navs />
          <StyledLefTopGradient />
          <StyledContainer>
            <CountryPicker handleCountryChange={this.handleCountryChange} />
            <CardData data={data} />
            <Chart data={data} country={country} />
          </StyledContainer>
          <StyledFooter>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 200">
              <path
                fill="#ec4357"
                fill-opacity="1"
                d="M0,128L24,138.7C48,149,96,171,144,186.7C192,203,240,213,288,208C336,203,384,181,432,186.7C480,192,528,224,576,208C624,192,672,128,720,106.7C768,85,816,107,864,106.7C912,107,960,85,1008,69.3C1056,53,1104,43,1152,80C1200,117,1248,203,1296,218.7C1344,235,1392,181,1416,154.7L1440,128L1440,320L1416,320C1392,320,1344,320,1296,320C1248,320,1200,320,1152,320C1104,320,1056,320,1008,320C960,320,912,320,864,320C816,320,768,320,720,320C672,320,624,320,576,320C528,320,480,320,432,320C384,320,336,320,288,320C240,320,192,320,144,320C96,320,48,320,24,320L0,320Z"
              ></path>
            </svg>
          </StyledFooter>
        </StyledBigContainer>
      </>
    );
    }
}

export default Statistique;