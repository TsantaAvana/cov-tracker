import React from "react";
import styled from "styled-components";
import Lottie from "react-lottie";
import { Navs } from "./nav";
import { Footer } from "./footer";
import * as doctorAnimation from "../lottie/covid19-doctor.json";
import * as stayAnimation from "../lottie/covid19-stay-at-home.json";
import { device } from "../style/device";
import { theme } from "../style/theme";
import { ExternalUrl } from "../utils/url";

const { fontSizes } = theme;
const StyledContainer = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  overflow: hidden;
  ${device.phone`
    position: relative;
    overflow: hidden;
   `};
  ${device.tiny`
    position: relative;
    overflow: hidden;
   `};
  ${device.phablet`
    position: relative;
    overflow: hidden;
   `};
`;

const StyledSectionIntro = styled.section`
  position: relative;
  overflow: hidden;
  min-height: 400px;
`;

const StyledSectionAbout = styled.section`
  position: relative;
  overflow: hidden;
  min-height: 400px;
`;

const StyledIntroWrapper = styled.div`
  display: flex;
  padding-left: 10%;
  ${device.tablet`
     display: flex;
     padding-left:5%;
   `};
  ${device.phone`
     display: block;
     padding-left:2%;
   `};
  ${device.tiny`
     display: block;
     padding-left:2%;
   `};
  ${device.phablet`
     display: block;
     padding-left:2%;
   `};
`;

const StyledAboutWrapper = styled.div`
  display: flex;
  padding-right: 10%;
  ${device.tablet`
     display: flex;
     padding-right:5%;
   `};
  ${device.phone`
        display: block;
        padding-left: 2%;
   `};
  ${device.tiny`
        display: block;
        padding-left: 2%;
   `};
  ${device.phablet`
        display: block;
        padding-left: 2%;
   `};
`;

const StyledIntroLeft = styled.div`
  position: relative;
  width: 500px;
  max-width: 400px;
  padding: 0 3em;
  ${device.tablet`
     width: 500px;
     padding: 0 3em;
   `};
  ${device.phone`
        width: 300px;
        padding: 0 1em;
    `};
  ${device.tiny`
        width: 300px;
        padding: 0 1em;
    `};
  ${device.megatiny`
        width: 250px;
        padding: 0 1em;
    
    `};
  ${device.phablet`
        width: 350px;
        padding: 0 1em;
    `};
`;

const StyledAboutLeft = styled.div`
  position: relative;
  width: 1000px;
  max-height: 400px;
  ${device.tablet`
     width: 1000px;
   `};
  ${device.phone`
         width: 340px;
         margin-left: 0;
    `};
  ${device.tiny`
         width: 340px;
         margin-left: 0;
    `};
  ${device.megatiny`
        width: 250px;
        padding: 0 1em;
    
    `};
  ${device.phablet`
         width: 380px;
         margin-left: 0;
    `};
`;

const StyledIntroRight = styled.div`
  position: relative;
  width: 1000px;
  max-height: 400px;
  ${device.tablet`
     width: 1000px;
     padding: 0 3em;
     height: 400px;
   `};
  ${device.phone`
        width:380px;
         margin-left: 0;
     `};
  ${device.tiny`
         width: 340px;
         margin-left: 0;
     `};
  ${device.megatiny`
        width: 250px;
        padding: 0 1em;
    
    `};
  ${device.phablet`
         width: 380px;
         margin-left: 0;
     `};
`;

const StyledAboutRight = styled.div`
  position: relative;
  width: 500px;
  max-width: 400px;
  padding: 0 3em;
  ${device.tablet`
     width: 500px; 
   `};
  ${device.phone`
        width: 300px;
        padding: 0 1em;
    `};
  ${device.tiny`
        width: 300px;
        padding: 0 1em;
    `};
  ${device.megatiny`
        width: 250px;
        padding: 0 1em;
    
    `};
  ${device.phablet`
        width: 350px;
        padding: 0 1em;
    `};
`;

const StyledIntroLeftTitle = styled.h1`
  font-size: 2.5em;
  position: relative;
  line-height: 1.5em;
  color: #241b47;
  margin-bottom: 1em;
  &:after {
    position: absolute;
    content: "";
    bottom: -24px;
    left: 0;
    width: 40px;
    height: 4px;
    border-radius: 4px;
    opacity: 0.4;
    background: #ec4357;
  }
`;

const StyledAboutRightTitle = styled.h1`
  font-size: 2.5em;
  position: relative;
  line-height: 1.5em;
  color: #ec4357;
  margin-bottom: 1em;
  &:after {
    position: absolute;
    content: "";
    bottom: -24px;
    left: 0;
    width: 40px;
    height: 4px;
    border-radius: 5px;
    opacity: 0.4;
    background: #241b57;
  }
`;

const StyledIntroLeftText = styled.p`
  font-weight: 500;
  line-height: 1.75em;
`;

const StyledAboutRightText = styled.p`
  font-weight: 500;
  line-height: 1.75em;
`;

const StyledIntroLeftLink = styled.a`
  display: block;
  width: 100%;
  max-width: 180px;
  text-align: center;
  margin-top: 2em;
  background: #ec4357;
  color: #ffffff;
  border-radius: 3px;
  padding: 1em;
  text-decoration: none;
`;

const StyledAboutRightLink = styled.a`
  display: block;
  width: 100%;
  max-width: 200px;
  text-align: center;
  margin: 2em 1em;
  background: #241b57;
  color: #ffffff;
  border-radius: 3px;
  padding: 1em;
  z-index: 99;
  font-weight: 600;
  text-decoration: none;
`;

const StyledIntroLefTopGradient = styled.div`
  position: absolute;
  top: -50%;
  right: -20%;
  width: 800px;
  height: 800px;
  border-radius: 50%;
  background: linear-gradient(#ee9ca7, #ec4357);
  ${device.tablet`
        position: absolute;
        width: 400px;
        height: 400px;
        top: -5%;
        right: -20%;

    `};
  ${device.phone`
        position: absolute;
        width: 300px;
        height: 300px;
        top: -5%;
        right: -20%;

    `};
  ${device.tiny`
        position: absolute;
        width: 300px;
        height: 300px;
        top: -5%;
        right: -20%;

    `};
  ${device.phablet`
        position: absolute;
        width: 300px;
        height: 300px;
        top: -5%;
        right: -20%;

    `};
`;

const StyledBanner = styled.div`
  background: #ec4357;
  text-align: center;
  padding: 50px;
  height: 200px;
  ${device.phone`
        height: 150px;
        padding: 20px;

    `};
  ${device.tiny`
        height: 150px;
        padding: 20px;

    `};
  ${device.phablet`
        height: 150px;
        padding: 20px;

    `};
`;

const StyledBannerTitle = styled.h1`
  color: #ffffff;
  font-size: 2.8em;
  font-weight: 500;
  ${device.phone`
        font-size: ${fontSizes.xl}

    `};
  ${device.tiny`
        font-size: ${fontSizes.xl}

    `};
  ${device.phablet`
        font-size: ${fontSizes.xl}

    `};
`;

const StyledBannerSubTitle = styled.h3`
  color: #ffffff;
  margin-bottom: 35px;
  font-weight: 300;
  ${device.phone`
        font-size: ${fontSizes.smish}

    `};
  ${device.tiny`
        font-size: ${fontSizes.smish}

    `};
  ${device.phablet`
        font-size: ${fontSizes.smish}

    `};
`;

const StyledBannerLink = styled.a`
  color: #ffffff;
  font-weight: 400;
  max-width: 400px;
  margin: 3em 0;
  padding: 1em 3em;
  text-align: center;
  border-radius: 3px;
  background: #241b57;
  text-decoration: none;
`;

const doctorOptions = {
  loop: true,
  autoplay: true,
  animationData: doctorAnimation.default,
  renderSettings: {
    preserveAspectRatio: "xMidYMid slice",
  },
};

const stayOptions = {
  loop: true,
  autoplay: true,
  animationData: stayAnimation.default,
  renderSettings: {
    preserveAspectRatio: "xMidYMid slice",
  },
};

export const Acceuil = () => {
  return (
    <>
      <StyledContainer>
        <Navs />
        <StyledIntroLefTopGradient></StyledIntroLefTopGradient>
        <StyledSectionIntro>
          <StyledIntroWrapper>
            <StyledIntroLeft>
              <StyledIntroLeftTitle>
                {" "}
                Qu'est que le COVID-19?
              </StyledIntroLeftTitle>
              <StyledIntroLeftText>
                Le coronavirus, qui tire leur nom à la forme de couronne qu’ont
                les protéines qui les enrobent, font partie d’une vaste famille
                de virus dont certains infectent différents animaux, d'autres
                l'homme
              </StyledIntroLeftText>
              <StyledIntroLeftLink
                href={ExternalUrl.articleCovid19}
                target="_blank"
                rel="nofollow noopener noreferrer"
                aria-label="ArticleCovid19"
              >
                Voir l'article
              </StyledIntroLeftLink>
            </StyledIntroLeft>

            <StyledIntroRight>
              <Lottie options={doctorOptions} />
            </StyledIntroRight>
          </StyledIntroWrapper>
        </StyledSectionIntro>

        <StyledSectionAbout>
          <StyledAboutWrapper>
            <StyledAboutLeft>
              <Lottie options={stayOptions} />
            </StyledAboutLeft>

            <StyledAboutRight>
              <StyledAboutRightTitle>
                {" "}
                Pourquoi rester chez soi?
              </StyledAboutRightTitle>
              <StyledAboutRightText>
                D'abord, parce que, les chercheurs sont unanimes à ce sujet : en
                l'absence de véritable traitement, en l'absence de vaccin, la
                distanciation sociale est à ce jour, le seul moyen efficace de
                freiner cette pandémie.
              </StyledAboutRightText>
              <StyledAboutRightLink
                href={ExternalUrl.articleRester}
                target="_blank"
                rel="nofollow noopener noreferrer"
                aria-label="ArticleRester"
              >
                Voir l'article
              </StyledAboutRightLink>
            </StyledAboutRight>
          </StyledAboutWrapper>
        </StyledSectionAbout>
      </StyledContainer>

      <StyledBanner>
        <StyledBannerTitle> Visitez mon site </StyledBannerTitle>
        <StyledBannerSubTitle> Plus de projet intéressant</StyledBannerSubTitle>
        <StyledBannerLink
          href={ExternalUrl.portfolio}
          target="_blank"
          rel="nofollow noopener noreferrer"
          aria-label="Portfolio"
        >
          Visiter
        </StyledBannerLink>
      </StyledBanner>

      <Footer />
    </>
  );
};

export default Acceuil;
