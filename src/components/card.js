import React from "react";
import {
  Card,
  CardContent,
  Typography,
  Grid,
  CardMedia,
} from "@material-ui/core";
import styled from "styled-components";
import CountUp from "react-countup";
import Lottie from "react-lottie";
import * as loadingImage from "../lottie/covid19-loading.json";
import coughingImage from "../img/coughing.png";
import healthImage from "../img/health2.png";
import deathImage from "../img/death.jpg";
import { device } from "../style/device";

const StyledContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  width: 100%;
  margin: 50px 0;
  ${device.tablet`
     width : 100%;
     flex-direction: column;
     margin-bottom: 10px;
   `};
`;

const StyledLoadingContainer = styled.div`
  width: 100%;
  height: 50vh;
`;

const StyledGridInfected = styled(Grid)`
  margin: 0 2% !important;
  ${device.tablet`
     width: 70%;
     margin: 20px 0 !important;
   `};
`;

const StyledGridRecovered = styled(Grid)`
  margin: 0 2% !important;
  ${device.tablet`
     width: 70%;
     margin: 20px 0 !important;
   `};
`;

const StyledGridDeaths = styled(Grid)`
  margin: 0 2% !important;
  ${device.tablet`
     width: 70%;
     margin: 20px 0 !important;
   `};
`;

const StyledCardMedia = styled(CardMedia)`
  height: 250px;
  width: 100%;
  ${device.tablet`
     height: 200px;
   `};
`;

const loadingOptions = {
  loop: true,
  autoplay: true,
  animationData: loadingImage.default,
  renderSettings: {
    preserveAspectRatio: "xMidYMid slice",
  },
};

export const CardData = ({
  data: { confirmed, recovered, deaths, lastUpdate },
}) => {
  if (!confirmed) {
    return (
      <>
        <StyledLoadingContainer>
          <Lottie options={loadingOptions} />
        </StyledLoadingContainer>
      </>
    );
  }
  return (
    <>
      <StyledContainer>
        <Grid container spacing={3} justify="center">
          <StyledGridInfected item component={Card} md={3}>
            <StyledCardMedia image={coughingImage} />

            <CardContent>
              <Typography color="textSecondary" gutterBottom>
                Inféctés
              </Typography>
              <Typography variant="h5">
                <CountUp
                  start={0}
                  end={confirmed.value}
                  duration={2.5}
                  separator=","
                />
              </Typography>
              <Typography color="textSecondary">
                {new Date(lastUpdate).toDateString()}
              </Typography>
              <Typography variant="body">
                Nombre d'inféctés du COVID19
              </Typography>
            </CardContent>
          </StyledGridInfected>

          <StyledGridRecovered item component={Card} md={3}>
            <StyledCardMedia image={healthImage} />

            <CardContent>
              <Typography color="textSecondary" gutterBottom>
                Rétablis
              </Typography>
              <Typography variant="h5">
                <CountUp
                  start={0}
                  end={recovered.value}
                  duration={2.5}
                  separator=","
                />
              </Typography>
              <Typography color="textSecondary">
                {new Date(lastUpdate).toDateString()}
              </Typography>
              <Typography variant="body">
                Nombre de rétablis du COVID19
              </Typography>
            </CardContent>
          </StyledGridRecovered>

          <StyledGridDeaths item component={Card} md={3}>
            <StyledCardMedia image={deathImage} />

            <CardContent>
              <Typography color="textSecondary" gutterBottom>
                Morts
              </Typography>
              <Typography variant="h5">
                <CountUp
                  start={0}
                  end={deaths.value}
                  duration={2.5}
                  separator=","
                />
              </Typography>
              <Typography color="textSecondary">
                {new Date(lastUpdate).toDateString()}
              </Typography>
              <Typography variant="body">Nombre de morts du COVID19</Typography>
            </CardContent>
          </StyledGridDeaths>
        </Grid>
      </StyledContainer>
    </>
  );
};

export default CardData;
