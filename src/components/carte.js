import React, {useState, useEffect} from 'react';
import MapView from './mapView';
import './map.css';
import { fetchLocation } from '../api/index';
import styled from 'styled-components';
import {device} from '../style/device';
import {Link} from 'react-router-dom';





const StyledBackLink = styled(Link)`
  position: absolute;
  top: 10px;
  left: 10%;
  display: block;
  text-decoration: none;
  z-index: 9999;
  width: 100%;
  max-width: 180px;
  text-align: center;
  margin-top: 2em;
  background: #ec4357;
  color: #ffffff;
  border-radius: 3px;
  padding: 1em;
  ${device.tablet`
   left:15%;
    
  `}
`;

export const Carte = () =>  {
    
     
    const[loactionArray, setLocationArray] = useState([]);

    useEffect(() => {
        
       const fetchAPI = async() => {
           setLocationArray(await fetchLocation());
       };
       fetchAPI();
       
    },[]);

        
    
        return (
          <>
            
              <StyledBackLink to="/"> Revenir </StyledBackLink>
           
            <div className="container">
              <MapView data={loactionArray} />
            </div>
          </>
        );
    
    
}

export default Carte;