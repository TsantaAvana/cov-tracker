import React from 'react';
import {Map, TileLayer, Popup, CircleMarker}  from 'react-leaflet';
import "leaflet/dist/leaflet.css";
import './map.css';
import "leaflet/dist/leaflet.css";
import styled from "styled-components";
import Lottie from "react-lottie";
import * as loadingImage from "../lottie/covid19-loading.json";



 const MapboxUrl =
          "https://api.mapbox.com/styles/v1/tiavinarakotobe/ckae13zzl0m3l1irtop7efri1/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoidGlhdmluYXJha290b2JlIiwiYSI6ImNrYWUxOGx5MjExbmIzMW84d3R3eGlzM2IifQ.jm70HJQdCwIznsbKq-fvSQ";



const StyledLoadingContainer = styled.div`
  width: 100%;
  height: 60vh;
  position: absolute;
  top: 20%;
`;

const loadingOptions = {
  loop: true,
  autoplay: true,
  animationData: loadingImage.default,
  renderSettings: {
    preserveAspectRatio: "xMidYMid slice"
  }
};




const MapView = ({data:{locations}}) =>  {
    
    React.useEffect(() => {
      const L = require("leaflet");

      delete L.Icon.Default.prototype._getIconUrl;

      L.Icon.Default.mergeOptions({
        iconRetinaUrl: require("leaflet/dist/images/marker-icon-2x.png"),
        iconUrl: require("leaflet/dist/images/marker-icon.png"),
        shadowUrl: require("leaflet/dist/images/marker-shadow.png")
      });
    }, []);

    const  locationArray  = locations;
    
    if(!locations){
       return (
         <>
           <StyledLoadingContainer>
             <Lottie options={loadingOptions} />
           </StyledLoadingContainer>
         </>
       );
    }
    
   
     
    
    const markerElements = locationArray.map(location => {
        /*
        const {
            id, country_code,country, province,
            coordinates:{ latitude, longitude},
            latest:{confirmed,recovered,deaths}
        } = location;
      */
         
        
        let title = location.country;
        if(location.province!== '' && location.province !== location.country){
            title = `${location.province}, ${location.country}`
        }
        

        return (
          <CircleMarker
            center={[
              location.coordinates.latitude,
              location.coordinates.longitude
            ]}
            fill="true"
            color="#ec4357"
            radius={10}
          >
            <Popup>
              <span>{title}</span>
              <br />
              <span>Inféctés:&nbsp;</span>
              <span>{location.latest.confirmed}</span>
              <br />
              <span>Rétablis:&nbsp;</span>
              <span>{location.latest.recovered}</span>
              <br />
              <span>Morts:&nbsp;</span>
              <span>{location.latest.deaths}</span>
            </Popup>
          </CircleMarker>
        );
    });

    return (
      <Map 
      className="map-view" 
      center={[51, 0]} 
      zoom={5}
      animate='true'
      boxZoom='true'
      dragging='true'
      touchZoom='true'
      viewport='viewport'
      >
        <TileLayer
          attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url={MapboxUrl}
        />
        {markerElements}
        
      </Map>
    );
    
  
};

export default MapView;

